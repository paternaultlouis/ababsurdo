---
title: Mathématiques et TPE
---
description: Idées et exemples pour intégrer les mathématiques dans un projet de TPE.
---
date: 2016-04-14
---
image: traffic-cone-in-snow.jpg
---
toc: 1
---
tags:

première S
TPE
---
body:

J'encadre et je suis régulièrement jury de [TPE](http://eduscol.education.fr/cid47789/tpe.html), et les mathématiques sont souvent laissées de côté par rapport aux autres disciplines. Comme encadrant, j'observe beaucoup de groupes qui préparent, réalisent et exploitent une expérience de SVT ou physique pendant dix semaines, avant de se demander un mois avant le rendu comment intégrer des mathématiques ; comme jury, j'observe souvent des candidats dont la partie mathématique présentée se résume à des statistiques non exploitées, un calcul anecdotique non maîtrisé, un graphique mal compris, etc.

Je reconnais qu'il peut être difficile d'intégrer des mathématiques dans un sujet de TPE. Cet article a pour but de présenter quelques pistes.

## Avertissement

### Bi-disciplinarité

La manière idéale d'inclure des mathématiques dans un TPE est, comme l'indiquent les textes officiels, de trouver une problématique qui soit explicitement bi-disciplinaire, comme les exemples ci-dessous (que j'ai vu traités).

- Des élèves ont étudié l'évolution de la couleur des yeux des individus d'une population donnée (dans un modèle simplifié où la couleur n'est définie que par un gène avec deux allèles possibles : bleu (récessif) ou marron (dominant)), et la probabilité que l'une des couleurs finisse par disparaître. La partie SVT est évidente, et la partie mathématique a consisté en des probabilités, et de l'algorithmique pour effectuer des simulations.
- D'autres élèves se sont demandés comment simuler la physique dans les jeux vidéos. En particulier, ils ont écrit un programme simulant des boules soumises à la gravité, qui rebondissent les unes contre les autres ou contre des murs. Ils ont utilisé la physique pour étudier les forces mises en jeu et les mettre en équation, l'informatique, et la méthode d'Euler (voir plus bas) et les produits scalaires pour la simulation en elle même.

A contrario, un sujet quasiment mono-disciplinaire, pour lequel les mathématiques sont au service de l'autre discipline est moins pertinent. J'ai par exemple assisté à la soutenance d'un travail sur l'homéopathie, dans lequel les mathématiques n'étaient utilisées que pour calculer la concentration de principe actif dans une solution au cours des dilutions successives. Suivant la « quantité » de mathématiques, et la manière dont elles sont incluses dans la production concrète et la présentation, cela peut passer, mais c'est moins bien qu'une problématique bi-disciplinaire.

### Exploitation des résultats

Quelles que soient les mathématiques mises en œuvre, les résultats doivent être exploités.

Cela peut paraître évident, mais les mathématiques ne doivent pas être hors-sujet. Par exemple, un sondage ne répond pas à la problématique « Comment fonctionne l'autotest ? ».

Les résultats doivent être interprétés. Un long raisonnement mathématique se concluant par $f(10)\approx 2,67$ ne sert pas à grand'chose. Le but d'un TPE est de répondre à une problématique : le raisonnement mathématique doit donc y apporter une réponse, et ce lien (entre les mathématiques et la problématique) doit être explicite. Par exemple, j'ai déjà assisté, en tant que jury de mathématique, à des présentations d'expérience de SVT dont la conclusion était « En regardant les tubes à essai, on voit que ça marche. », et je n'ai rien compris. De même, un raisonnement de mathématique doit avoir une conclusion en français, et un lien avec la problématique qu'un non-mathématicien soit en mesure de comprendre.

## Exemples

Voici quelques outils mathématiques abordables en première S, et pouvant être utilisés en TPE.

### Statistiques et Échantillonnage

Énumérer des estimateurs statistiques provenant de sources diverses, ce n'est pas des mathématiques. En revanche, déterminer ces indicateurs par des expériences, un sondage, les représenter en utilisant la représentation appropriée, et surtout les exploiter (c'est-à-dire, par exemple, prendre des décisions), c'est un travail mathématique. Se poser la question de la précision de ces indicateurs, en utilisant par exemple des intervalles de confiance (vu en seconde dans le chapitre sur l'échantillonnage), c'est un travail mathématique. Réaliser une simulation l'est également.

Au sujet des intervalles de confiance, la formule vue en seconde[^confiance] nécessite un échantillon de grande taille pour fournir des réponses significatives. Suivant l'expérience mise en jeu, produire de tels échantillons peuvent être hors de portée d'élèves dans le cadre d'un TPE. Pour contourner cette limitation, il est alors possible de constater que les résultats sont non-significatifs, puis de calculer quelle devrait être la taille de l'échantillon pour obtenir des résultats significatifs (en supposant vouloir reproduire l'expérience).

[^confiance]: $\left[f-\frac{1}{\sqrt{n}};f+\frac{1}{\sqrt{n}}\right]$

### Algorithmique

L'algorithmique fait partie du programme de mathématiques. Réaliser un programme pour simuler une expérience, déterminer un intervalle de confiance, etc., est donc bienvenu dans le cadre d'un TPE.

### Résolution d'équations différentielles

Lors de l'étude de modèles physiques, des équations différentielles sont souvent rencontrées :

- la [chute libre](https://fr.wikipedia.org/wiki/Chute_libre_%28physique%29) ;
- plusieurs phénomènes, comme le comportement d'un bâtiment face à un séisme, peuvent être modélisés par un [système masse ressort](https://fr.wikipedia.org/wiki/Syst%C3%A8me_masse-ressort) ;
- l'effet de [l'accélération de Coriolis](https://fr.wikipedia.org/wiki/Oscillation_d'inertie) sur les courants marins ;
- quantité d'un médicament ou d'une hormone dans le corps humain ;
- etc.

Les équations différentielles ne sont pas au programme, et il faut donc être motivé pour les utiliser en TPE. Mais j'ai vu des groupes faire du bon travail avec. Plusieurs méthodes peuvent être utilisées pour les résoudre.

#### Méthode d'Euler

Avec [un peu d'investissement](http://debart.pagesperso-orange.fr/1s/methode_euler.html), la [méthode d'Euler](https://fr.wikipedia.org/wiki/M%C3%A9thode_d'Euler) est utilisable. Elle peut être mise en œuvre à l'aide d'un tableur ou d'un programme informatique écrit par les élèves.

#### « Intervention divine »

Pour des élèves ne souhaitant pas s'attaquer à la méthode d'Euler, ou pour des problèmes pour lesquels elle est peu efficace[^resonnance], il est possible d'utiliser ce que j'appelle une résolution « par intervention divine ». Étant donné notre équation différentielle, on trouve une fonction (sur internet, dans un rêve, donnée par un cousin prof de math, etc.), et on vérifie ensuite que cette fonction est bien une solution de l'équation considérée.

[^resonnance]: Par exemple, je n'ai pas réussi à modéliser [un système masse ressort avec oscillations forcées](https://fr.wikipedia.org/wiki/Syst%C3%A8me_oscillant_%C3%A0_un_degr%C3%A9_de_libert%C3%A9#Oscillations_forc.C3.A9es), la variation d'un système entretenu étant plus faible que l'erreur de la méthode d'Euler.

### Probabilités inversées

C'est au programme de terminale, mais sans parler du [théorème de Bayes](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_de_Bayes), il est possible d'utiliser les probabilités inversées.

Par exemple, sachant que pour l'[autotest VIH](https://www.sida-info-service.org/?Les-autotests-VIH-sur-Sida-Info), les essais sur quelques centaines de personnes ont produit 0% de faux négatifs et 0,2% de faux positifs, en considérant, par la loi des grands nombres, ces statistiques comme des probabilités, et en [estimant à 150 000](http://www.franceinter.fr/depeche-sida-150-000-francais-contamines) le nombre de français contaminés, quelle est la probabilité qu'une personne dont l'autotest est positif soit réellement séropositive ?

### Géométrie du triangle

Les programmes du collège (et du lycée avec le théorème d'Al Kashi) couvrent de nombreux aspects de la géométrie du triangle. Dés lors, de nombreux calculs peuvent être faits en utilisant ces connaissances, comme le calcul du périmètre ou de l'aire d'un polygone régulier connaissant son côté ou le rayon de son cercle inscrit ou circonscrit, etc.

### Et bien d'autres…
