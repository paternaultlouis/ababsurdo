#!/usr/bin/env python3

# Copyright 2019 Louis Paternault
#
# This is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Génère un graphe avec certaines contraintes, puis affiche le résultat sous forme de code Graphviz.

Pour utiliser :

python3 pagerank.py | circo -T pdf -o graphe.pdf
"""

import functools
import random

ORDRE = 6
DEGRE_ENTRANT = functools.partial(random.choice, [1, 2, 2, 2, 3])
PAGERANK_N = 1000000

class Sommet:
    def __init__(self, *, nom=None):
        self.extremites = list()
        self.origines = list()
        self.pagerank = 0
        if nom is None:
            self.nom = "S" + str(id(self))
        else:
            self.nom = nom

class Graphe:
    def __init__(self, sommets):
        self.sommets = list(sommets)

    def __iter__(self):
        yield from self.sommets

    def __len__(self):
        return len(self.sommets)

    def ajoute_arete(self, origine, extremite):
        """Ajoute une arête"""
        origine.extremites.append(extremite)
        extremite.origines.append(origine)

    @classmethod
    def aleatoire(cls):
        """Génère un graphe aléatoire."""
        noms = iter("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        graphe = cls(Sommet(nom=next(noms)) for _ in range(ORDRE))

        # Chaque sommet a au moins une arête sortante
        for origine, extremite in zip(graphe, random.sample(list(graphe), k=len(graphe))):
                graphe.ajoute_arete(origine, extremite)

        # Chaque sommet a exactement DEGRE_ENTRANT arêtes entrantes
        for extremite in graphe:
            for _ in range(DEGRE_ENTRANT() - len(extremite.origines)):
                origine = random.choice([sommet for sommet in graphe if (sommet != extremite and sommet not in extremite.origines)])
                graphe.ajoute_arete(origine, extremite)

        return graphe

    def calcule_pagerank(self):
        """Mets à jour les pagerank de tous les sommets."""
        sommet = random.choice(self.sommets)
        for _ in range(PAGERANK_N):
            sommet = random.choice(sommet.extremites)
            sommet.pagerank += 1/PAGERANK_N

    def __str__(self):
        """Produit le code Graphviz du graphe."""
        texte = ""
        texte += """digraph graphe {\n"""
        for origine in self:
            for extremite in origine.extremites:
                texte += """{} -> {};\n""".format(origine.nom, extremite.nom)
        for sommet in self:
            texte += """{}[label="{:.2f}"];\n""".format(sommet.nom, sommet.pagerank)
        texte += """}\n"""
        return texte

def cherche():
    """Cherche un graphe vérifiant un certain nombre de conditions."""

    def cond_boucles(graphe):
        """Il n y a au maximum une boucle."""
        boucles = 0
        for sommet in graphe:
            if sommet in sommet.extremites:
                boucles += 1
        return boucles <= 1

    def cond_degre3(graphe):
        """Il y a au moins un sommet de degré entrant supérieur ou égal à 3."""
        for sommet in graphe:
            if len(sommet.origines) >= 3:
                return True
        return False

    def cond_degre2(graphe):
        """Il y a au moins un sommet de degré entrant supérieur ou égal à 2, avec un pagerank d'au moins 20%."""
        for sommet in graphe:
            if len(sommet.origines) == 2 and sommet.pagerank >= .2:
                return True
        return False

    while True:
        graphe = Graphe.aleatoire()
        graphe.calcule_pagerank()
        for condition in [cond_boucles, cond_degre2, cond_degre3]:
            if not condition(graphe):
                continue
        return graphe

if __name__ == "__main__":
    print(cherche())

