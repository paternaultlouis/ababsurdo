---
title: Échantillonnage et Zététique en seconde
---
description: Utilisation d'une expérience (fictive) d'un sourcier, pour introduire la notion d'échantillonnage en secondes.
---
date: 2015-08-20
---
image: kilchurn-castle-loch-awe.jpg
---
toc: 1
---
tags:

seconde
loi binomiale
probabilités
statistiques
zététique
---
body:

*Remarque : Une version plus récente de cet article [est disponible](../20190115-sourcier).*

Ce document s'adresse à des professeurs de mathématiques de lycée, afin qu'il soit enrichi et réutilisé dans leurs classes. Il décrit une séance faite avec une classe de secondes, utilisant la zététique comme support pour aborder la notion d'échantillonnage.

## Objectifs

### Mathématiques

Cette séance introduit l'ensemble de partie du programme de seconde générale qui concerne
l'échantillonnage, comme par exemple :  « Exploiter et faire une analyse
critique d'un résultat d'échantillonnage. »

### Zététique

Cette séance vise à montrer comment l'échantillonnage permet de porter un regard critique sur la société qui nous entoure, et en particulier sur les pseudo-sciences. En particulier, le but est d'introduire la maxime « La charge de la preuve est à celui ou celle qui affirme. »

- Cet objectif s'inscrit également dans le cadre du programme officiel, en participant à « donner à chaque élève la culture mathématique indispensable pour sa vie de citoyen ».
- Cet activité permet également de poursuivre le développement de la compétence du socle commun : « L'appréhension rationnelle des choses développe les attitudes suivantes : […] l'esprit critique : distinction entre le prouvé, le probable ou l'incertain, la prédiction et la prévision, situation d'un résultat ou d'une information dans son contexte […]. »

## Contexte

### Mathématiques

Cette séance a eu lieu fin décembre, pendant le chapitre sur les statistiques.
Les élèves avaient donc vu (avec moi la semaine précédente, ou au collège) des
notions de statistiques descriptives (moyenne, médiane, quartiles,
représentations graphiques). L'échantillonnage, en revanche, était nouveau pour
eux.

Ils n'avaient quasiment pas utilisé de calculatrice scientifique.

### Zététique

Je n'avais jamais abordé ce type de sujet, et ils n'avaient (à ma connaissance)
jamais fait ou entendu parler de zététique.

## Séances

Cette activité s'est déroulée en plusieurs temps.

### Veille

J'avais donné aux élèves, comme consigne de devoir à la maison, de trouver des preuves que le Père Noël n'existe pas (en leur précisant que, bien que l'énoncé soit surprenant, j'étais sérieux). Ceci a suscité la curiosité de quelques élèves, à qui j'ai expliqué que nous allions travailler sur la notion de preuve.

### Père Noël et Charge de la preuve

Au début de la séance, j'écris au tableau l'affirmation « Le Père Noël existe », et je demande aux élèves de me prouver le contraire. Extraits de dialogues :

> **Élève :** Ça n'est pas possible de visiter toutes les maisons du monde en une nuit. Il faudrait qu'il dépasse la vitesse de la lumière / son traîneau aurait un poids démesuré / vu la vitesse nécessaire, à cause de la friction de l'air, son traîneau prendrait feu / il ne peut pas livrer des cadeaux dans les maisons sans cheminées…    
> **Prof :** Le Père Noël est magique : il n'est donc pas soumis aux lois de la physique.    
> **Élève :** Mais la magie n'existe pas !    
> **Prof :** Prouvez le moi.

 

> **Élève :** Si le Père Noël existait, il apporterait des cadeaux à tout le monde, or les enfants pauvres n'ont pas de cadeaux.    
> **Prof :** Le Père Noël n'aime pas les pauvres.

 

> **Élève :** Mais la magie n'existe pas. Vous avez déjà vu une licorne ?    
> **Prof :** Vous avez déjà vu un rhinocéros ?

Tous les élèves n'ont pas participé à cet échange, mais un bon nombre a essayé
d'apporter des preuve. J'ai senti la frustration des élèves, de qui je balayais
toutes les tentatives de preuves, ce qui montre leur implication dans
l'exercice.

Un élève a finalement remarqué que que je n'avais qu'à prouver que le Père Noël
existe, réflexion que j'ai reprise, et qui m'a permis d'écrire et d'expliquer
la maxime « La charge de la preuve est à celui ou celle qui affirme », que j'ai
ensuite illustrés avec d'autres exemples (« la nuit dernière, j'ai été enlevé puis relâché par des extra-terrestres »). Je
n'ai pas mentionné (et les élèves non plus) que le même raisonnement s'applique
exactement de la même manière si l'on remplace le Père Noël par Dieu.

### Sourcier et Échantillonnage

J'ai ensuite expliqué que nous utilisons la preuve en mathématiques pour
démontrer plein de choses, mais jusqu'à maintenant, dans leurs cours de
mathématiques, ils ne s'en sont servi, dans la grande majorité, que pour des énoncés mathématiques. Le
but de la séance est d'introduire un outil permettant de prouver des énoncés
« de la vraie vie ».

J'ai ensuite introduit le cas d'étude suivant : « Une personne affirme être
sourcier, c'est-à-dire avoir le pouvoir de détecter des sources d'eau. Comment
faire pour confirmer ou informer son prétendu don ? » Peu à peu, l'idée de
mettre le sourcier à l'épreuve a émergé, qui devrait être faite en aveugle (je
n'ai pas abordé la notion de double aveugle), et enfin, nous avons convenu
qu'il fallait répéter cette épreuve, pour limiter l'intervention du hasard (une
version plus développée de cette démarche est décrite dans [*Esprit critique,
es-tu là
?*](http://www.book-e-book.com/index.asp?sessionID=336258136&fx=2&p_id=180) par
le [collectif CorteX](http://cortecs.org/ateliers/esprit-critique-es-tu-la/), ou
par Stanislas Antczak and Florent Tournus sur [le site de l'Observatoire
Zététique](http://www.zetetique.fr/index.php/cours/44-supports/143-animation-scientifique#note9)).

Nous n'avons pas réalisé l'expérience dans la classe, mais j'ai présenté les
résultats (calculés pour être à la limite de l'intervalle de fluctuation à 95%,
tel qu'étudié en seconde) : sur les 50 essais, notre sourcier a eu 31 bonnes
réponses. Comment interpréter ce résultat ?

Après d'autres réflexions, nous avons convenu que la question était : une telle
réussite peut-elle être attribuée au hasard, ou est-elle la preuve d'un don ?
Il nous fallait donc simuler plusieurs expériences, pour voir s'il nous
arrivait d'atteindre 31 réussites sur 50 essais.

Chaque table d'élève a ensuite utilisé sa calculatrice pour simuler une série
de 50 essais, avec une probabilité de réussite de 50%, et compilé les résultats
au tableau, sur un axe gradué de 0 à 50. Manque de chance, ou erreurs
d'utilisation de la calculatrice (voir la section *Problèmes et améliorations
envisagées*), sur une vingtaine de simulation, à peine deux ou trois ont
dépassé les 25 succès, et nous avons du conclure, à mon grand regret, qu'autant
de succès avaient vraiment peu de chances d'être attribués au hasard, et que le
« sourcier » avait sans doute des dons.

### Intervalle de fluctuation

La dernière phase de l'activité a pris la forme d'un cours magistral plus
classique. Après avoir expliqué l'intérêt d'un tel outil (notamment par rapport
aux simulations), j'ai présenté l'intervalle de fluctuation
$\left[p-\frac{1}{\sqrt{n}};p+\frac{1}{\sqrt{n}}\right]$ et son utilisation.
Après l'avoir appliqué à notre sourcier, nous avons enfin conclu qu'il n'avait
pas donné la preuve de ses pouvoirs.

## Problèmes et améliorations envisagées

- Lorsque les élèves devaient me prouver que le Père Noël n'existe pas, je
  réfutais moi-même leurs arguments. Il pourrait être intéressant de leur
  laisser le temps de les réfuter eux-mêmes.
- C'est un problème technique, mais tout de même important. C'était la première
  fois que nous utilisions le générateur aléatoire sur leurs calculatrices
  neuves : elles généraient donc toutes la même séquence. Ne sachant pas, à
  l'époque, comment définir la graine du générateur, je leur ai dit de passer
  un certain nombre de premières valeurs, mais il est peu probable que cela ait
  suffit. D'autre part, j'ai peut-être manqué de précisions dans mes
  instructions pour générer des nombres aléatoires, puisque j'ai vu au moins
  deux élèves écrire sur leur calculatrice quelque chose comme
  ``0.3Rand()``, ce qui a fait grandement baisser le taux de réussite de
  notre simulation. Une solution aurait été d'utiliser, au vidéo-projecteur, un
  [émulateur de
  calculatrice](http://www.maths-et-tiques.fr/index.php/outils-pour-le-prof/emulateurs-calculatrices).

## Téléchargement

- Énoncé : [Diapo](sourcier-diapo.pdf), [élève](sourcier-eleve.pdf).
- [Sources](sourcier.bz2)
