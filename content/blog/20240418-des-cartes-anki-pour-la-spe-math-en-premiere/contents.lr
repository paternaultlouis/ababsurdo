---
title: Des cartes Anki pour la spécialité mathématiques en première générale
---
description: Voici un jeu de cartes Anki pour le programme de math de spécialité en première générale
---
date: 2024-04-18
---
image: 209.jpg
---
mastodon_id: 112293763992720297
---
toc: 1
---
tags:

Anki
première générale
spécialité
---
body:

J'ai créé tout au long de l'année un jeu de cartes [Anki](https://apps.ankiweb.net/) couvrant l'ensemble du programme de la spécialité mathématiques en première générale. C'est par ici :

- [Présentation sur mon site web](https://paternaultlouis.forge.apps.education.fr/anki-math/)
- [Partage sur Ankiweb](https://ankiweb.net/shared/info/940066280?cb=1713473962962)

J'ai présenté ce travail en tout début d'année à mes élèves. Au moins l'un d'entre eux a essayé de l'utiliser, sans succès (à cause d'un problème technique non résolu, à cause du jeu de carte ou de son application). J'ai un peu laissé tombé pour cette année, mais je vais essayer de faire mieux l'an prochain. Je pense que les autoriser à installer l'application et importer ce jeu de cartes pendant un cours peu permettre de corriger immédiatement certains problèmes (et cela peut se faire pendant une [séance de travail en autonomie](../20230814-plan-de-travail-en-premiere-et-seconde/)).

Ce jeu de cartes est évidemment proposé sous licence [Creative Commons by-sa 4.0](http://creativecommons.org/licenses/by-sa/4.0/deed.fr). Toutes les remarques, corrections, améliorations, sont les bienvenues !
