---
title: Échantillonnage, Loi binomiale et Zététique
---
description: Utilisation de la loi binomiale pour analyser les prévisions d'un astrologue, en première.
---
date: 2015-01-10
---
image: astrologie.png
---
toc: 1
---
tags:

première S
loi binomiale
probabilités
zététique
---
body:


*Remarque : Une version plus récente de cet article [est disponible](../20150110-sismologie).*

*Merci à [Guillemette Reviron (du CorteX)](http://cortecs.org) pour ses conseils et relectures. Cet article est également publié [sur le site du CorteX](http://cortecs.org/exercices/echantillonnage-loi-binomiale-et-zetetique-par-louis-paternault/).*

Ce document s'adresse à des professeurs de mathématiques de lycée, afin qu'il soit enrichi et réutilisé dans leurs classes. Il décrit une séance faite avec une classe de première S, utilisant la zététique comme support pour aborder la notion d'échantillonnage avec la loi binomiale. Les mêmes notions étant également au programme de première ES et L, il doit être possible de l'adapter pour ces classes.

## Téléchargement

- [Énoncé](sismologie.pdf) ([source](sismologie.tex))
- [Corrigé](sismologie-corrige.pdf) ([source](sismologie-corrige.tex))
- [Feuille de calcul](sismologie.ods) obtenue par les élèves à la fin de l'activité.

## Objectifs

### Mathématiques

- Cette séance introduit la partie du programme concernant l'échantillonnage : la capacité attendue du programme est « exploiter l'intervalle de fluctuation à un seuil donné, déterminé à l'aide de la loi binomiale, pour rejeter ou non une hypothèse sur une proportion. »
- Elle permet du travail en salle informatique, exploitant le tableur au service de la recherche mathématique.

### Zététique

Cette séance vise à montrer comment les outils mathématiques vus en classe de première (en particulier l'échantillonnage) permettent de porter un regard critique sur la société qui nous entoure, et en particulier sur les pseudo-sciences.

- Cet objectif s'inscrit également dans le cadre du programme officiel, en participant à « donner à chaque élève la culture mathématique indispensable pour sa vie de citoyen ».
- Cet activité permet également de poursuivre le développement de la compétence du socle commun : « L'appréhension rationnelle des choses développe les attitudes suivantes : […] l'esprit critique : distinction entre le prouvé, le probable ou l'incertain, la prédiction et la prévision, situation d'un résultat ou d'une information dans son contexte […]. »

## Contexte

### Professionnel

Je n'ai fait cette activité qu'une seule fois ; je la referai si mon affectation le permet. Je n'ai donc pas encore pu améliorer cette activité en prenant en compte l'évaluation de ces deux séances.

Ce travail a eu lieu pendant mon année de stage où ma gestion de classe était en cours d'acquisition. Ceci, ajouté à la période de l'année où a eu lieu ce travail (avant-dernière et dernière séances), fait que l'attention de mes élèves était loin d'être maximale.

### Mathématiques

Cette séance a eu lieu en toute fin d'année (avant dernière séance, et une tentative de bilan en dernière séance). Elle a pris place en dernière partie du dernier chapitre de la progression (Loi binomiale). Les élèves connaissaient donc la loi binomiale, même si ce savoir était tout récent.

Ils manipulaient pour la première fois le tableur cette année (mais ils l'avaient déjà manipulé les années précédentes).

### Zététique

Je n'avais jamais abordé ce type de sujet, et ils n'avaient (à ma connaissance) jamais fait ou entendu parler de zététique.

## Séances

Cette activité s'est déroulée en deux temps.

Une première séance a eu lieu en demi-groupes, en salle informatique. Les élèves travaillaient à deux sur un poste, et ont suivi l'énoncé, en prenant des notes dans leur cahier d'exercices. Je leur ai laissé peu de temps de réflexion pour la première partie, que nous avons vite corrigée ensemble, le but étant de leur laisser davantage de temps sur les parties suivantes. Nous sommes arrivés au bout de la troisième partie, et je leur ai distribué un support de cours pour institutionnaliser les savoirs vus dans la séance. Malheureusement, par manque de temps, la troisième partie a été trop vite faite : la question 1 a été survolée, et nous n'avons pas pris le temps de réfléchir à la troisième question.

Un bilan de cette séance a été fait au début de la séance suivante. J'ai présenté à nouveau la démarche, ajoutant la courbe de Gauß pour visualiser de manière différente les résultats, et ré-expliquer les conclusions.

## Évaluation

Cette activité est très adaptée aux deux objectifs (mathématiques et zététique). Malheureusement, il est souvent difficile de proposer du contenu non strictement mathématique par manque de temps. Ces séances, en revanche, utilisent la zététique au service des notions mathématiques du programme, et l'aspect esprit critique ne constitue pas une « perte de temps » par rapport au reste du programme.

Les élèves ont assez peu réagi au travail effectué, et je ne suis pas sûr que les notions de zététiques aient été acquises, pour deux raisons. D'une part, comme dit plus haut, l'attention n'était pas très bonne. D'autre part, le contenu mathématique abordé était assez dense pour une seule séance, donc il devait rester assez peu de disponibilité d'esprit pour des éléments qui peuvent paraître secondaires aux yeux des élèves.

L'aspect zététique de ce travail aurait pu donner lieu à des discussions. Le cadre du bilan, qui a eu lieu pendant la dernière séance de l'année, aurait pu être l'occasion de discuter davantage. J'ai vu que la conclusion en double négation (« Ce travail ne permet pas de conclure que les prédictions ne sont pas dues au hasard ») a posé problème, sans pouvoir y répondre. Cela aurait pourtant été l'occasion d'introduire la règle disant que « la charge de la preuve incombe à celui ou celle qui prétend ».

## Améliorations envisagées

Voici ce que je modifierai sans doute lorsque je referai cette activité.

- Le travail doit être étalé sur plus de séances, pour être moins dense et que l'aspect zététique soit davantage acquis. La première partie de l'activité peut être faite plus en autonomie, puisqu'elle ne met en jeu que des savoirs qui n'ont rien de nouveau par rapport au reste du chapitre. Pour alléger la séance informatique, il pourra être utile de leur faire faire ce travail en devoir à la maison (évalué si c'est une motivation nécessaire), ou lors d'une séance précédente, en classe. Le bilan peut être l'occasion de discussions ou de travaux supplémentaires sur la zététique. La forme et le contenu sont laissés au lecteur patient.
- Dans la seconde question de la troisième partie, on passe des effectifs aux fréquences pour manipuler les données. Bien que cela corresponde au programme, il pourra être utile de ne pas manipuler des fréquences, puisque leur utilisation introduit un calcul supplémentaire qui ajoute encore un peu de complexité.
- Pour travailler la notion de preuve, il pourra être utile de demander aux élèves, en amont ou en aval de ce travail, de [prouver la non-existence du Père-Noël](../20150820-sourcier), pour leur montrer que tous leurs arguments peuvent être réfutés, qu'il n'est pas possible de prouver la non-existence de quelque chose, et donc que la charge de la preuve incombe à celui ou celle qui affirme.
