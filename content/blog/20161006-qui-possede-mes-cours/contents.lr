---
title: Qui possède mes cours ?
---
description: Je publie mes cours sous licence libre, mais en ai-je le droit ? Est-ce moi ou l'État qui possède la propriété intellectuelle sur mes productions ?
---
date: 2016-10-06
---
image: propriete_privee.jpg
---
toc: 1
---
tags:

droit d'auteur
loi
---
body:


L'ensemble de mes productions (cours, devoirs, etc.) [est publié sous licence libre](pourquoi_publier_sous_licence_libre) ; d'autres collègues vendent les leurs sous forme de livres. En avons-nous le droit ? Ne devrions nous pas demander l'autorisation à l'État pour disposer de nos œuvres ? Inversement, l'État a-t-il le droit de les utiliser sans notre permission ?

## Avertissement

Même si je pense connaître le droit d'auteur mieux que la moyenne, je ne suis pas juriste. Il est donc tout à fait possible que ce document contienne, au mieux, des inexactitudes, et au pire, de grosses âneries. À croire à vos risques et péril.

## Que dit la loi

Regardons notre [contrat de travail](http://legifrance.gouv.fr).

### Droit moral

Notre droit moral est un peu affaibli, dans la mesure où nous ne pouvons pas *« [nous] opposer à la modification de l'oeuvre décidée dans l'intérêt du service par l'autorité investie du pouvoir hiérarchique, lorsque cette modification ne porte pas atteinte à [notre] honneur ou à [notre] réputation »* ([article L. 121-7-1](https://www.legifrance.gouv.fr/affichCodeArticle.do;?idArticle=LEGIARTI000006278900&cidTexte=LEGITEXT000006069414&dateTexte=vig), merci à [Didier Frochot](http://www.les-infostrateges.com/article/060418/le-droit-d-auteur-des-agents-publics)). Autrement dit, dans une certaine mesure, nos supérieurs hiérarchiques peuvent modifier notre travail sans notre accord.

### Droit patrimonial

[L'article L. 111-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006278868&cidTexte=LEGITEXT000006069414&dateTexte=vig) stipule que *« l'auteur d'une oeuvre de l'esprit jouit sur cette oeuvre, du seul fait de sa création, d'un droit de propriété incorporelle exclusif et opposable à tous »*, ainsi qu'il *« n'est pas non plus dérogé à la jouissance de ce même droit lorsque l'auteur de l'oeuvre de l'esprit est un agent de l'État [...]. »* Autrement dit, nous (professeurs) sommes seuls maîtres de nos œuvres, et l'État n'a aucun pouvoir.

Sauf que…

[L'article L. 131-3-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006278959&dateTexte=20160930) ajoute que *« dans la mesure strictement nécessaire à l'accomplissement d'une mission de service public, le droit d'exploitation d'une oeuvre créée par un agent de l'État dans l'exercice de ses fonctions ou d'après les instructions reçues est, dès la création, cédé de plein droit à l'État. »* Donc le moindre de mes textes écrit pour mes élèves est la propriété de l'État, et je n'ai pas le droit de l'utiliser sans son accord. Donc une bonne partie de ce site web est illégale.

Sauf que…

La fin de [l'article L. 111-1](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069414&idArticle=LEGIARTI000006278868&dateTexte=20161006&categorieLien=cid#LEGIARTI000006278868) précise que *« les dispositions des articles (...) L. 131-3-1 (...) ne s'appliquent pas aux agents auteurs d'oeuvres dont la divulgation n'est soumise, en vertu de leur statut ou des règles qui régissent leurs fonctions, à aucun contrôle préalable de l'autorité hiérarchique. »* Donc, si nos supérieurs hiérarchiques ne contrôlent pas nos écrits avant nos cours, nous sommes libres d'en faire ce que nous voulons.

## Conclusion

Pour le droit patrimonial (le droit de publier sous licence libre, d'en faire un livre, de gagner de l'argent avec, etc.), deux cas se présentent :

* nos productions (cours, devoirs, etc.) n'étant pas soumises à validation de l'autorité hiérarchique (nos inspecteurs ne vérifient pas chacun des textes distribués à nos élèves), nous en gardons le droit de propriété intellectuelle. Donc j'ai le droit de publier sur ce site web tous mes cours, et les collègues qui vendent des livres en ont aussi le droit ;
* les sujets d'examens (baccalauréat par exemple), en revanche, puisqu'ils sont relus, modifiés et validés par l'autorité hiérarchique, ne nous appartiennent plus, mais sont cédés totalement à l'État.
