title: Pourquoi je publie mes travaux sous licence libre
---
description: Tous mes travaux sont publiés sous une licence Creative-Commons by-sa, qui est très permissive. Pourquoi autoriser la réutilisation à tou·te·s de mon travail ?
---
date: 2014-11-19
---
image: mass-production.jpg
---
toc: 2
---
tags:

droit d'auteur
loi
---
body:

{% from 'jinjamacros/images.html' import iframe with context %}

Je publie l'ensemble de ce que je produis dans le cadre de mon travail (cours,
sujets de devoirs, etc.) en utilisant une licence libre : la licence [Creative
Commons by-sa](http://creativecommons.org/licenses/by-sa/4.0/deed.fr). Cela
signifie que j'autorise n'importe qui à :

- télécharger, lire, étudier mon travail ;
- l'utiliser (en cours par exemple) ;
- le modifier, l'améliorer ;
- le redistribuer.

Les contraintes principales étant :

- ne pas s'attribuer mon travail (c'est-à-dire ne pas prétendre que vous en
  êtes l'auteur), et plus généralement, laisser mon nom apposé à mes travaux
  (c'est une contrainte imposée par le droit français : même si j'étais
  d'accord pour que quelqu'un d'autre prétende être l'auteur de mon travail, je
  n'en aurais pas le droit ) ;
- en cas de republication (et je pense qu'une utilisation en classe est
  considérée comme une publication), le document doit être publié sous la même
  licence. Cela signifie qu'un professeur utilisant mon travail et l'améliorant
  ne peut interdire qu'il soit à son tour partagé, utilisé et amélioré par
  d'autres.

## Pourquoi pas ?

Le principal argument à publier mon travail sous licence libre, est simplement
que cela peut aider des collègues, et que je ne vois aucune raison de ne pas le
faire.

Pourquoi l'interdirais-je ? Autrement dit : En quoi cela me porte-t-il
préjudice qu'un collègue utilise mon travail, sans que j'en tire (a priori, j'y
reviendrai) un bénéfice quelconque ?

### Avantage financier

> En publiant gratuitement mon travail, je travaille sans rémunération.

Je suis professeur, donc je suis déjà payé pour
créer ces contenus pédagogiques : il ne me parait donc pas légitime de demander
une seconde rémunération.
De plus, en tant que professeur de l'enseignement public, c'est l'État qui me
paye pour produire ce travail : ces contenus pédagogiques sont donc un bien
(immatériel) commun, qui appartient à l'ensemble des citoyens, et que personne
(pas même moi) ne doit privatiser, ce qui serait le cas si je revendiquais un
monopole d'exploitation sur ce travail.

Toutefois, le travail de mise en forme et de publication de mon travail n'est
pas compris dans mon service, et c'est donc un travail que je fournis
bénévolement. J'expliquerai dans la partie suivante pourquoi je suis content de
fournir ce travail.

> En publiant et autorisant la réutilisation de mon travail, je me prive de
> potentiels revenus futurs, si jamais je voulais vendre un livre contenant mon
> travail.

D'abord, je n'ai pas l'intention de vendre un livre sur le sujet.

Quand bien même je voudrais, même s'il parait évident qu'en publiant
gratuitement ce travail, je ne pourrais pas en vendre une version papier, c'est
loin d'être prouvé, et de nombreux exemples
montrent le contraire : c'est le cas d'auteurs déjà connus qui diffusent une œuvre en autorisant son partage non commercial, et qui pourtant font d'excellentes ventes (comme par exemple
[Radiohead](http://www.numerama.com/magazine/10864-l-experience-radiohead-a-fait-un-enorme-carton-financier.html)
ou
[Nine Inch Nails](http://ecrans.liberation.fr/ecrans/2009/01/06/nine-inch-nails-gratuit-et-best-seller_949471?page=article)),
ou d'auteurs qui se sont fait connaître en diffusant gratuitement des œuvres
sur internet, avant de gagner de l'argent en vendant une version physique de
ces mêmes œuvres (comme par exemple l'auteure de bande-dessinée Laurel, qui [a réussi à récolter plus de 350 000 €](http://fr.ulule.com/comme-convenu-2) pour
une version papier d'une œuvre
[pourtant librement accesible](http://commeconvenu.com)).

Enfin, j'expliquerai dans la partie suivante les bénéfices que je tire de ce
partage.

### Travailler pour les autres

> En publiant ce travail, j'offre des week-ends de repos à des collègues qui
> l'utiliseront, tandis que je travaille sérieusement.

Premièrement, je doute que ce soit vrai : par expérience, m'approprier le
travail d'un collègue ne me prend pas sensiblement moins de temps qu'utiliser
mon propre travail. Chacun a son style, ses manières de faire ; chacun a une
progression différente, introduit les notions de manières différentes ; et
finalement, un travail que je produis, qui s'intègre parfaitement dans mon
enseignement, devra être retravaillé pour s'intégrer dans le cours d'un
collègue.

Il m'est arrivé quelques fois, pris par le temps, d'utiliser le travail d'un
collègue sans grande préparation, et les résultats n'ont pas été très bons. Un
collègue faisant du mauvais travail en utilisant le mien fera aussi du mauvais
travail sans utiliser le mien, donc la publication ou non de mon contenu
pédagogique n'est pas pertinente.

Enfin, si je comprends le sentiment d'injustice que pourrait ressentir un
professeur travaillant à ses cours, et voyant un de ses collègues réutiliser
son travail pour passer moins de temps pour fournir un résultat d'aussi bonne
qualité, refuser de publier mes cours pour cette raison (c'est-à-dire pour que
tout le monde passe autant de temps que moi à préparer ses cours) me paraît
bien dommage : c'est forcer tous les travailleurs à fournir un travail, non pas
parce qu'il est utile, mais pour justifier un salaire. Il est facile de
partager une partie du travail que les professeurs fournissent (par exemple
[pour ma matière](tags/aide_mémoire)), et, si cela permet de réduire le
temps de travail global des professeurs, c'est une bonne chose.

### Responsabilité et Réputation

> Internet ne vous rend pas plus stupide : il permet seulement au monde entier
> de s'en rendre compte.

C'est, personnellement, le plus gros frein que je vois à cette publication. En
tant que professeur, à peu de choses près, seuls mes élèves voient mon travail,
et peuvent donc le juger. En publiant mon travail, je m'expose donc à ce que
d'autres collègues voient ce que je fais, voient surtout les erreurs que je
fais.
Une petite dose d'humilité est nécessaire pour accepter cela.

Une des raisons pour lesquelles je suis professeur de mathématiques est pour
partager un savoir mathématique. Si mon travail est bon, qu'il soit utilisé
par des collègues avec leurs élèves : ainsi, indirectement, je partage mon
savoir mathématique avec encore plus d'élèves. Si mon travail est mauvais,
qu'il soit utilisé et analysé dans la formation des professeurs pour montrer ce
qu'il ne faut pas faire : ça n'est pas la manière la plus glorieuse de
participer au partage du savoir mathématiques, mais si ça marche…

{% from 'jinjamacros/images.html' import image with context %}
{{ image("me-citation.png") }}

## Pourquoi ?

La principale raison pour laquelle publier mes travaux sous une licence libre
est que je ne vois pas de raisons de ne pas le faire. Mais il y a également
d'excellentes raisons de le faire.

### Partage de pratiques

Je ne suis pas l'unique auteur de ma pratique pédagogique : elle est constituée
de mon expérience en tant qu'élève durant toute ma scolarité, d'échanges avec
mes collègues (ou d'autres personnes), de mes lectures, des formations que j'ai
suivies, et j'ai seulement mélangé tout cela à ma sauce, et ajouté ma touche
personnelle.

De la même manière que j'ai bénéficié des contributions de nombreuses
personnes, je souhaite aider à mon tour, et faire profiter l'ensemble de la
communauté éducative de mon travail. Cela me semble être un juste retour des
choses.

Ça n'est pas un échange direct (je ne « rends » pas un exercice intéressant à
un collègue qui m'avait donné un exercice intéressant), mais indirect : j'ai
puisé mes pratiques dans celles de la communauté, j'autorise donc la communauté
à puiser dans mes pratiques.

### Contribution aux savoirs communs

En me considérant ici non seulement comme un professeur, mais plus
généralement, comme un auteur de contenus culturels, en publiant mon travail
— et surtout, en autorisant sa réutilisation — je participe (à mon humble
niveau) à la connaissance commune.

{{ iframe(
  "http://player.vimeo.com/video/96746925",
  caption='<a href="https://vimeo.com/96746925">TEDxGeneva 2014</a>, de <a href="http://www.gwennseemel.com">Gwenn Seemel</a>.',
) }}

Il n'y a pas de création sans copie (voir la vidéo ci-dessus) ; il n'y a pas de
création originale. Toute création se fait avec la réutilisation d'éléments
appartenant à la culture commune (par exemple, un genre musical n'est rien
d'autre que les créations d'un ensemble de compositeurs qui se copient les uns
les autres).
Si tous les auteurs interdisent la réutilisation de leurs œuvres (jusqu'à
soixante-dix ans après leur mort, quand elles s'élèveront dans le domaine
public), cela empêche l'ensemble des autres auteurs de créer à leur tour.

En revanche, si tous les auteurs partagent leurs œuvres, autorisent leur
réutilisation, cela profite à la culture commune, et à tous. C'est pour cette
raison que je partage mes œuvres.

## Pour aller plus loin

Quelques liens pour approfondir le sujet du droit d'auteur, du partage de
savoir, de la création culturelle…

- [Un article](http://www.framasoft.net/article4399.html) de Michèle Drechsler, inspectrice de l'Éducation nationale, présente un état des lieux de l'utilisation des licences libres dans l'éducation.
- La conférence [En défense de l'imitation](http://www.gwennseemel.com/index.php/blog/comments-fr/avec_soustitres/) (incluse dans l'article), de [Gwenn Seemel](http://www.gwennseemel.com) analyse le lien entre création et copie, et explique qu'il n'y a pas de copie sans création, et pas de création sans copie.
- Dans [Internet & rémunération des auteurs](http://www.tierslivre.net/spip/spip.php?article1865), François Bon présente une analyse de la rémunération des auteurs à l'heure du numérique.
- J'ai présenté une conférence [Le piratage c'est du vol et autres idées reçues](http://podcast.grenet.fr/episode/atelier-n72-la-trepidante-histoire-du-droit-dauteur-1-le-piratage-cest-du-vol-et-autres-phrases-chocs/), analysant quelques idées reçues concernant le droit d'auteur.
- [Framasoft](http://www.framasoft.org) est une association, à l'origine issue du monde enseignant, dédiée à la promotion du « libre » (le logiciel à l'origine, mais aussi la [culture libre](http://www.framasoft.net/#topPgCulture) en général).
