TEX := $(shell find content -type f -regex ".*\.tex")
ODT := $(shell find content -type f -regex ".*\.odt")
ODS := $(shell find content -type f -regex ".*\.ods")
ODG := $(shell find content -type f -regex ".*\.odg")
ODP := $(shell find content -type f -regex ".*\.odp")
PDF := $(TEX:.tex=.pdf) $(ODT:.odt=.pdf) $(ODS:.ods=.pdf) $(ODG:.odg=.pdf) $(ODP:.odp=.pdf)
MAKEFILES := $(shell find content -type f -name Makefile)

# Où aller chercher les paquets LaTeX
export TEXINPUTS:=$(TEXINPUTS):$(PWD)/other/latex/

all: build

.PHONY: MAKE

.ONESHELL:
MAKE:
	for makefile in $(MAKEFILES)
	do
		(cd $$(dirname $$makefile) && make)
	done


%.pdf: %.tex
	spix $<

%.pdf: %.ods
	cd $(@D) && libreoffice --headless --convert-to pdf $(<F)

%.pdf: %.odt
	cd $(@D) && libreoffice --headless --convert-to pdf $(<F)

%.pdf: %.odg
	cd $(@D) && libreoffice --headless --convert-to pdf $(<F)

%.pdf: %.odp
	cd $(@D) && libreoffice --headless --convert-to pdf $(<F)

build: MAKE $(PDF)
	lektor build --output-path public

serve: MAKE $(PDF)
	lektor serve

.ONESHELL:
loop:
	# J'obtiens souvent des segmentation fault…
	while true
	do
		make serve
	done

quick:
	lektor build --output-path public
