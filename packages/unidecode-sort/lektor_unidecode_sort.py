"""Udd unidecode_sort() and unidecode_dictsort() filters.

Most of it has been copied-pasted from the sort() and dictsort() jinja2 filters.
https://github.com/pallets/jinja/blob/a24df26d54fa2ccbe9bdaa0bb9419075a00e2699/src/jinja2/filters.py
"""

import typing as t

from unidecode import unidecode

from jinja2.filters import ignore_case, make_multi_attrgetter
from jinja2.utils import pass_environment
from lektor.pluginsystem import Plugin

K = t.TypeVar("K")
V = t.TypeVar("V")


@pass_environment
def do_unidecode_sort(
    environment: "Environment",
    value: "t.Iterable[V]",
    reverse: bool = False,
    case_sensitive: bool = False,
    attribute: t.Optional[t.Union[str, int]] = None,
) -> "t.List[V]":
    key_func = make_multi_attrgetter(
        environment, attribute, postprocess=ignore_case if not case_sensitive else None
    )

    def unidecode_key_func(value):
        return list(unidecode(item) for item in key_func(value))

    return sorted(value, key=unidecode_key_func, reverse=reverse)


def do_unidecode_dictsort(
    value: t.Mapping[K, V],
    case_sensitive: bool = False,
    by: 'te.Literal["key", "value"]' = "key",
    reverse: bool = False,
) -> t.List[t.Tuple[K, V]]:
    if by == "key":
        pos = 0
    elif by == "value":
        pos = 1
    else:
        raise FilterArgumentError('You can only sort by either "key" or "value"')

    def sort_func(item: t.Tuple[t.Any, t.Any]) -> t.Any:
        value = item[pos]

        if not case_sensitive:
            value = ignore_case(value)

        return unidecode(value)

    return sorted(value.items(), key=sort_func, reverse=reverse)


class UnidecodeSortPlugin(Plugin):
    name = "Unidecode Sort"
    description = "Add unidecode_sort() and unidecode_dictsort() filters."

    def on_setup_env(self, **extra):
        self.env.jinja_env.filters["unidecode_sort"] = do_unidecode_sort
        self.env.jinja_env.filters["unidecode_dictsort"] = do_unidecode_dictsort
